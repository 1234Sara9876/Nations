﻿namespace Nations.Serviços
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    class NetworkService
    {
        public Response CheckConnection()
        {
            var client = new WebClient();
            try
            {
                //endereço da google
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return new Response
                    {
                        //se using WebClient.OpenRead (acesso aos dados do URL) passar o try, isto é, não tiver erros ao decorrer
                        IsSuccess = true
                    };
                }
            }
            catch
            {
                //se ocorrer erro no acesso ao URL
                return new Response
                {
                    IsSuccess = false,
                    Message = "Configure a sua ligação à internet"
                };
            }
        }
    }
}
