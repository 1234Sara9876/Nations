﻿namespace Nations.Serviços
{
    using System.Windows.Forms;

    class DialogService
    {
        //classe cuja finalidade é mostrar mensagens no decorrer do programa
        public void ShowMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
