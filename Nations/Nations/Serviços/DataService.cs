﻿namespace Nations.Serviços
{
    using System;
    using System.Collections.Generic;
    using Modelos;
    using System.Data.SQLite;
    using System.IO;

    public class DataService
    {
        //classe que cria, preenche e manipula a base de dados SQL (em SQLite) 
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogService dialog;

        public DataService()
        {
            dialog = new DialogService();
            //cria a B.D. caso não exista ainda (primeira execução do programa)
            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }
            //@indica que se vai mostrar o caminho da pasta da B.D.,que por defeito será gravada junto ao ficheiro executável Nations (na mesma pasta deste)
            var path = @"Data\Nation.sqlite";

            try
            {
                //liga à base de dados
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open();
                //cria as tabelas correspondentes às listas List<Nation>, List<Currency> e topLevelDomain
                string sqlcommand = "create table if not exists nations(idnation integer primary key, name varchar(250), capital varchar(250), region varchar(250), population integer)";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                sqlcommand =  "create table if not exists currencies(idcurrency integer primary key autoincrement, code varchar(250), name varchar(250), symbol char(2), idnation integer, foreign key (idnation) references nations (idnation))";
                //SQLiteCommand.ExecuteNonQuery() aplica na B.D. a instrução que acabou de se criar
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                sqlcommand = "create table if not exists domains(iddomain integer primary key autoincrement, topleveldomain varchar(5), idnation integer, foreign key (idnation) references nations (idnation))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialog.ShowMessage("Erro", e.Message);
            }
        }

        public void DeleteData()
        {
            //apaga primeiro os dados para voltar a preencher as tabelas cada vez que chegam os dados actualizados da api
            try
            {
                string sqlcommand = string.Format("delete from nations; delete from currencies; delete from domains; update sqlite_sequence set seq = 0");
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialog.ShowMessage("Erro", e.Message);
            }
        }

        public void SaveData(List<Nation> PaisesCopiados)
        {//método que guarda os dados actualizados da api na B.D. local(para ficar também actualizada)
            try
            {
                int index = 1;

                foreach (var origem in PaisesCopiados)
                {
                    //para cada registo da lista países, que passou do programa principal para este método  como parâmetro
                    string sqlcommand =                  
                        string.Format("insert into nations (idnation, name, capital, region, population)values(@idnation,@name,@capital,@region,@population)");
                    //SQLiteCommand.Parameters.AddWithValue preenche os campos(colunas) da tabela de nações com os valores especificados
                    command = new SQLiteCommand(sqlcommand, connection);
                    command.Parameters.AddWithValue("@idnation", index);
                    command.Parameters.AddWithValue("@name", origem.name);
                    command.Parameters.AddWithValue("@capital", origem.capital);
                    command.Parameters.AddWithValue("@region", origem.region);
                    command.Parameters.AddWithValue("@population", origem.population);

                    command.ExecuteNonQuery();

                    //preencher dados na tabela de moedas
                    foreach (var moedas in origem.currencies)
                    {
                        sqlcommand = string.Format("insert into currencies (code, name, symbol, idnation) values (@code, @name, @symbol, @idnation)");

                        command = new SQLiteCommand(sqlcommand, connection);
                        command.Parameters.AddWithValue("@code", moedas.code);
                        command.Parameters.AddWithValue("@name", moedas.name);
                        command.Parameters.AddWithValue("@symbol", moedas.symbol);
                        command.Parameters.AddWithValue("@idnation", index);

                        command.ExecuteNonQuery();
                    }

                    //Guardar dados na tabela dominio
                    foreach (var dominio in origem.topLevelDomain)
                    {
                        sqlcommand = string.Format("insert into domains(topleveldomain, idnation) values (@topleveldomain, @idnation)");

                        command = new SQLiteCommand(sqlcommand, connection);
                        command.Parameters.AddWithValue("@topleveldomain",dominio);
                        command.Parameters.AddWithValue("@idnation",index);

                        command.ExecuteNonQuery();
                    }
                    index++;
                }
                connection.Close();
            }
            catch (Exception e)
            {
                dialog.ShowMessage("Erro", e.Message);
            }
        }

        public List<Nation> GetData()
        {
            //método que carrega os dados da B.D. local na aplicação em caso de não haver internet
            List<Nation> paises = new List<Nation>();

            try
            {
                string sql = "select idnation, name, capital, region, population from nations";
                command = new SQLiteCommand(sql, connection);
                //SQLiteCommand.ExecuteReader() cria uma instância da classe SQLiteDataReader
                SQLiteDataReader reader = command.ExecuteReader();
                //SQLiteDataReader.Read() lê os registos da tabela seleccionada um a um
                while (reader.Read())
                {
                    //o método Add adiciona novos registos à lista declarada e instanciada neste método
                    //a coluna currencies é preenchida através de um método que retorna a lista de moedas
                    //a coluna topLevelDomain é preenchida através de um método que retorna a lista de domínios
                    paises.Add(new Nation { name = (string)reader["name"], capital = (string)reader["capital"], region = (string)reader["region"], population = Convert.ToInt32(reader["population"]), currencies = GetCurrencies(Convert.ToInt32(reader["idnation"])), topLevelDomain = GetTLDomain(Convert.ToInt32(reader["idnation"])) });
                }
                return paises;
            }
            catch (Exception exc)
            {
                dialog.ShowMessage("ErroGetData", exc.Message);
                return null;
            }
        }

        //método que retorna a lista de moedas
        public List<Currency> GetCurrencies(int idnat)
        {
            List<Currency> moedas = new List<Currency>();

            string sql1 = "select code, name, symbol from currencies where idnation=" + idnat;
            command = new SQLiteCommand(sql1, connection);
            SQLiteDataReader reader1 = command.ExecuteReader();
            while (reader1.Read())
            {
                string rname;
                string rcode;
                string rsymbol;

                if (reader1["name"] == DBNull.Value)
                    rname = string.Empty;
                else
                    rname = (string)reader1["name"];
                if (reader1["code"] == DBNull.Value)
                    rcode = string.Empty;
                else
                    rcode = (string)reader1["code"];
                if (reader1["symbol"] == DBNull.Value)
                    rsymbol = string.Empty;
                else
                    rsymbol = (string)reader1["symbol"];

                moedas.Add(new Currency { code = rcode, name = rname, symbol = rsymbol });
            }
            return moedas;
        }

        //método que retorna a lista de domínios
        public List<string> GetTLDomain(int idnat)
        {
            List<string> dominios = new List<string>();

            string sql1 = "select topleveldomain from domains where idnation=" + idnat;
            command = new SQLiteCommand(sql1, connection);
            SQLiteDataReader reader1 = command.ExecuteReader();
            while (reader1.Read())
            {
                dominios.Add((string)reader1["topleveldomain"]);
            }
            return dominios;
        }
    }
}
