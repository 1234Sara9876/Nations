﻿namespace Nations.Serviços
{
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    class ApiService
    {
        //tarefa assíncrona porque aguarda resposta a uma transmissão desse género (é chamada através da instrução await, no programa principal)
        public async Task<Response> GetNations(string urlBase, string controller)
        {
            try
            {
                //HttpClient envia e recebe dados através do protocolo da web, http, recebendo o endereço urlBase
                //HttpClient.GetAsync recebe os dados do controlador da api
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.GetAsync(controller);
                //o conteúdo da transmissão é lido em string
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }
                //converte a resposta "deserializada" do código Json numa lista de objetos da classe Nation: a lista países
                var paises = JsonConvert.DeserializeObject<List<Nation>>(result);
                return new Response
                {
                    IsSuccess = true,
                    Result = paises
                };
                //Result passou a ser um objeto do tipo lista
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
