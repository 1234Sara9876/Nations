﻿namespace Nations.Modelos
{
    using System.Collections.Generic;

    public class Nation
    {
        //classe cujos atributos foram gerados através da tradução de Json para C Sharp,porque a requisição à api
        //é respondida através do envio de dados em formato Json ; estes dados foram copiados da aplicação Postman,
        // onde se consultou o endereço da api para observar os dados, e traduzidos pela aplicação Json2CSharp
        public string name { get; set; }
        public string capital { get; set; }
        public string region { get; set; }
        public int population { get; set; }
        public List<string> topLevelDomain { get; set; }
        public string flag { get; set; }
        public List<Currency> currencies { get; set; }

    }
}
