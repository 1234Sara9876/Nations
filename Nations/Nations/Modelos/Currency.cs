﻿namespace Nations.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Currency
    {
        //classe que contém os atributos da lista currencies pertencente à classe nations
        public string code { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }

        //este método aplica o polimorfismo isto é a implementação diferente do mesmo método (o original ToString() )em diferentes programas que usam a classe que o contém 
        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}", code, name, symbol);
        }
    }
}
