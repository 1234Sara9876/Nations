﻿using System.Net.NetworkInformation;

namespace Nations
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Threading.Tasks;
    using Modelos;
    using Serviços;
    using System.IO;
    using System.Net;
    using System.Drawing.Imaging;
    using System.Drawing;
    using Svg;
    

    public partial class Nations : Form
    {
        #region Atributos

        private SvgDocument imagem;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;
        private List<Nation> Paises;
        private List<string> TopLevelDomain;
        bool nat;

        #endregion

        #region Construtor do form

        public Nations()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            Paises = new List<Nation>();
            TopLevelDomain = new List<string>();
            imagem = new SvgDocument();
            LoadPaises();
        }

        #endregion

        #region Métodos de extracção dos dados 

        private async void LoadPaises()
        {
            //indica aos utilizadores que aguardem pela obtenção dos dados
            lblData.Text = DateTime.Now.ToString();
            lblProgresso.Text = "A atualizar países...";
            //verifica se há internet
            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                //se não houver internet carrega os dados da base de dados local
                LoadLocalPaises();
            }
            else
            {
                //se houver internet extrai os dados da api que faz a ligação  à base de dados restcountries
                await LoadApiPaises();
                //armazena os ficheiros svg de cada link correspondente ao atributo flag 
                GetBandeiras();
            }
            if (Paises==null)
            {
                //validação que verifica se a lista de países foi de facto preenchida
                lblProgresso.Text = "Não há ligação à internet \nAs nações não foram carregadas \nTente mais tarde";
                return;
            }
            //lança a lista de países na ComboBox
            cbOrigem.DataSource = Paises;
            cbOrigem.DisplayMember = "name";
            cbOrigem.SelectedIndex = -1;


            //indica aos utilizadores que já podem ver os países
            lblProgresso.Text = "Nações atualizadas...";
            lblData.Text = string.Format("Nações carregadas da internet em {0:F}", DateTime.Now);
            pbActualiza.Value = 100;
        }

        private void LoadLocalPaises()
        {
            Paises=dataService.GetData();
        }

        private async Task LoadApiPaises()
        {
            //coloca a progressBar no início
            pbActualiza.Value = 0;
            //faz a requisição à api
            var response = await apiService.GetNations("https://restcountries.eu","/rest/v2/all");
            //recebe os dados requisitados na forma de uma lista 
            Paises = (List<Nation>)response.Result;
            //actualiza a base de dados local
            dataService.DeleteData();
            dataService.SaveData(Paises);
        }

        private void GetBandeiras()
        {
            //cria o directório de armazenamento dos ficheiros svg
            if (!Directory.Exists("Bandeiras"))
            {
                Directory.CreateDirectory("Bandeiras");
            }
            try
            {
                foreach (Nation origem in Paises)
                {
                    //indica o caminho para o local de armazenamento
                    string caminhoBandeiras = Application.StartupPath + "\\Bandeiras\\" + origem.name;
                    //instancia a classe que requisita à web o conteúdo de um link (o campo-atributo flag)
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(origem.flag, caminhoBandeiras);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        #endregion

        #region Métodos de apresentação dos dados

        //método desencadeado quando se clica na ComboBox
        private void cbOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {
            //limpa os dados do primeiro elemento da lista obtida, pois o utilizador ainda não escolheu
            if (cbOrigem.SelectedIndex == -1)
            {
                tbCapital.Text = "";
                tbRegiao.Text = "";
                tbPopul.Text = "";
                pbBandeira.Image=null;
                lvMoeda.Items.Clear();
                dgvDomains.Columns.Clear();
                dgvDomains.Rows.Clear();
                return;
            }
            //cria a variável que recebe a nação escolhida pelo utilizador, para mostrá-la
            var thisNation = (Nation)cbOrigem.SelectedItem;
            nat = true;
            if (nat)
            {
                //mostra os elementos unitários da lista (porque a lista também contém listas)
                tbRegiao.Text = thisNation.region;
                tbPopul.Text = thisNation.population.ToString();
                tbCapital.Text = thisNation.capital;
               
                //Application.startUpPath dá o caminho da pasta que contém o ficheiro executável que inicia esta aplicação Nations (pasta Debug dentro da pasta Bin, na pasta Nations), que é onde estão as imagens das bandeiras - caminhoImagem indica onde está o .svg que corresponde à bandeira da nação escolhida
                string caminhoImagem = Application.StartupPath + "\\Bandeiras\\" + thisNation.name;
                try
                {
                    //cria a variável para onde se carrega o conteúdo do ficheiro .svg (imagem vectorial)
                    imagem = SvgDocument.Open(caminhoImagem);
                    //transforma esse conteúdo num bitmap (imagem formada por pixels)
                    Bitmap imagemCopiada = imagem.Draw();
                    //grava o bitmap como ficheiro .png 
                    imagemCopiada.Save(caminhoImagem + ".png", ImageFormat.Png);
                    //lança a imagem do ficheiro .png na pictureBox
                    pbBandeira.Image = imagemCopiada;
                    //apaga o ficheiro
                    File.Delete(caminhoImagem + ".png");
                }
                catch
                {
                    MessageBox.Show("Não é possível visualizar a bandeira", "Erro de conversão");
                }
                //mostra a lista de moedas da nação (pode ter uma ou mais) - ao clicar escreve por extenso
                foreach (Currency c in thisNation.currencies)
                {
                    ListViewItem line;
                    line = lvMoeda.Items.Add(c.ToString());
                }
                //mostra a lista de domínios internet (um ou mais) do país
                TopLevelDomain = thisNation.topLevelDomain;
                dgvDomains.Columns.Add("colDominio", "Domínio");
                for (int i = 0; i < TopLevelDomain.Count; i++)
                    dgvDomains.Rows.Add(thisNation.topLevelDomain[i]);
                dgvDomains.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
        }

        #endregion

       
       
    }
}

