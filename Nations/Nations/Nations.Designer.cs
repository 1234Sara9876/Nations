﻿namespace Nations
{
    partial class Nations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Nations));
            this.pbActualiza = new System.Windows.Forms.ProgressBar();
            this.lblData = new System.Windows.Forms.Label();
            this.lblProgresso = new System.Windows.Forms.Label();
            this.cbOrigem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblValor = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPopul = new System.Windows.Forms.TextBox();
            this.tbCapital = new System.Windows.Forms.TextBox();
            this.tbRegiao = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmSobre = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvDomains = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.lvMoeda = new System.Windows.Forms.ListView();
            this.pbBandeira = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomains)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBandeira)).BeginInit();
            this.SuspendLayout();
            // 
            // pbActualiza
            // 
            this.pbActualiza.Location = new System.Drawing.Point(488, 609);
            this.pbActualiza.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbActualiza.Name = "pbActualiza";
            this.pbActualiza.Size = new System.Drawing.Size(211, 23);
            this.pbActualiza.TabIndex = 20;
            // 
            // lblData
            // 
            this.lblData.Location = new System.Drawing.Point(98, 556);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(601, 28);
            this.lblData.TabIndex = 19;
            this.lblData.Text = "Data";
            // 
            // lblProgresso
            // 
            this.lblProgresso.Location = new System.Drawing.Point(212, 609);
            this.lblProgresso.Name = "lblProgresso";
            this.lblProgresso.Size = new System.Drawing.Size(248, 17);
            this.lblProgresso.TabIndex = 18;
            this.lblProgresso.Text = "Actualizado";
            this.lblProgresso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbOrigem
            // 
            this.cbOrigem.FormattingEnabled = true;
            this.cbOrigem.Location = new System.Drawing.Point(251, 49);
            this.cbOrigem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbOrigem.Name = "cbOrigem";
            this.cbOrigem.Size = new System.Drawing.Size(421, 24);
            this.cbOrigem.TabIndex = 15;
            this.cbOrigem.SelectedIndexChanged += new System.EventHandler(this.cbOrigem_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(96, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Região";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(99, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "P A Í S";
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(455, 420);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(122, 17);
            this.lblValor.TabIndex = 11;
            this.lblValor.Text = "Moeda corrente";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(95, 300);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 22;
            this.label3.Text = "População";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(99, 362);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Capital";
            // 
            // tbPopul
            // 
            this.tbPopul.BackColor = System.Drawing.SystemColors.Window;
            this.tbPopul.Location = new System.Drawing.Point(253, 298);
            this.tbPopul.Margin = new System.Windows.Forms.Padding(4);
            this.tbPopul.Name = "tbPopul";
            this.tbPopul.ReadOnly = true;
            this.tbPopul.Size = new System.Drawing.Size(421, 22);
            this.tbPopul.TabIndex = 25;
            // 
            // tbCapital
            // 
            this.tbCapital.BackColor = System.Drawing.SystemColors.Window;
            this.tbCapital.Location = new System.Drawing.Point(254, 360);
            this.tbCapital.Margin = new System.Windows.Forms.Padding(4);
            this.tbCapital.Name = "tbCapital";
            this.tbCapital.ReadOnly = true;
            this.tbCapital.Size = new System.Drawing.Size(420, 22);
            this.tbCapital.TabIndex = 27;
            // 
            // tbRegiao
            // 
            this.tbRegiao.BackColor = System.Drawing.SystemColors.Window;
            this.tbRegiao.Location = new System.Drawing.Point(251, 241);
            this.tbRegiao.Margin = new System.Windows.Forms.Padding(4);
            this.tbRegiao.Name = "tbRegiao";
            this.tbRegiao.ReadOnly = true;
            this.tbRegiao.Size = new System.Drawing.Size(421, 22);
            this.tbRegiao.TabIndex = 28;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSobre});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(745, 28);
            this.menuStrip1.TabIndex = 29;
            // 
            // tsmSobre
            // 
            this.tsmSobre.Name = "tsmSobre";
            this.tsmSobre.Size = new System.Drawing.Size(349, 24);
            this.tsmSobre.Text = "Trabalho realizado por Sara Cardoso, versão 1.0.0";
            // 
            // dgvDomains
            // 
            this.dgvDomains.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDomains.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDomains.Location = new System.Drawing.Point(98, 441);
            this.dgvDomains.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDomains.Name = "dgvDomains";
            this.dgvDomains.ReadOnly = true;
            this.dgvDomains.Size = new System.Drawing.Size(144, 85);
            this.dgvDomains.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(99, 420);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 17);
            this.label6.TabIndex = 31;
            this.label6.Text = " Domínios internet";
            // 
            // lvMoeda
            // 
            this.lvMoeda.Location = new System.Drawing.Point(330, 441);
            this.lvMoeda.Margin = new System.Windows.Forms.Padding(4);
            this.lvMoeda.Name = "lvMoeda";
            this.lvMoeda.Size = new System.Drawing.Size(369, 85);
            this.lvMoeda.TabIndex = 33;
            this.lvMoeda.UseCompatibleStateImageBehavior = false;
            // 
            // pbBandeira
            // 
            this.pbBandeira.BackColor = System.Drawing.Color.Transparent;
            this.pbBandeira.Location = new System.Drawing.Point(348, 92);
            this.pbBandeira.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbBandeira.Name = "pbBandeira";
            this.pbBandeira.Size = new System.Drawing.Size(229, 126);
            this.pbBandeira.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbBandeira.TabIndex = 34;
            this.pbBandeira.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(95, 139);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Bandeira";
            // 
            // Nations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(745, 652);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pbBandeira);
            this.Controls.Add(this.lvMoeda);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvDomains);
            this.Controls.Add(this.tbRegiao);
            this.Controls.Add(this.tbCapital);
            this.Controls.Add(this.tbPopul);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pbActualiza);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblProgresso);
            this.Controls.Add(this.cbOrigem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblValor);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Nations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Países do Mundo";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomains)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBandeira)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ProgressBar pbActualiza;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblProgresso;
        private System.Windows.Forms.ComboBox cbOrigem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPopul;
        private System.Windows.Forms.TextBox tbCapital;
        private System.Windows.Forms.TextBox tbRegiao;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmSobre;
        private System.Windows.Forms.DataGridView dgvDomains;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lvMoeda;
        private System.Windows.Forms.PictureBox pbBandeira;
        private System.Windows.Forms.Label label4;
    }
}

